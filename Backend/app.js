const express = require('express')
const mongoose = require('mongoose');
require('dotenv').config()
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')

const authRoute = require('./routes/auth')
const userRoute = require('./routes/user')
const categoryRoute = require('./routes/category')
const productRoute = require('./routes/product')
const orderRoute = require('./routes/order')


mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() => {
    console.log("DB CONNECTED..................")
})


const app = express()
const port = process.env.PORT || 8000

//this is the middleware
app.use(bodyParser.json())
app.use(cookieParser())
app.use(cors())

app.use('/api', authRoute)
app.use('/api', userRoute)
app.use('/api',categoryRoute)
app.use('/api',productRoute)
app.use('/api',orderRoute)



app.listen(port, () => {
    console.log(`Server is running at port ${port}...`)
})